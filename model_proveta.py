"""Create the test shape model."""

import matplotlib.pyplot as plt

from planner import fill, gcode
from shapely.geometry import Point, Polygon


def rectangle(xo, yo, xf, yf):
    """Create rectangle polygon."""
    return Polygon(((xo, yo), (xo, yf), (xf, yf), (xf, yo)))


def cercle(x, y, r):
    """Create circle polygon."""
    return Point((x, y)).buffer(r)

if __name__ == '__main__':
    # Create test shape
    fig = rectangle(0, 0, 94, 25)
    corba1 = cercle(26.5, -16.5, 25)
    corba2 = cercle(26.5, 25 + 16.5, 25)
    corba3 = cercle(94 - 26.5, -16.5, 25)
    corba4 = cercle(94 - 26.5, 25 + 16.5, 25)
    rect1 = rectangle(26.5, -16.5, 94 - 26.5, 8.5)
    rect2 = rectangle(26.5, 16.5, 94 - 26.5, 25 + 16.5)
    fig = fig.difference(corba1)
    fig = fig.difference(corba2)
    fig = fig.difference(corba3)
    fig = fig.difference(corba4)
    fig = fig.difference(rect1)
    fig = fig.difference(rect2)

    # Get contour
    xc = fig.exterior.xy[0]
    yc = fig.exterior.xy[1]
    xc = xc[1:] + xc[:1]
    yc = yc[1:] + yc[:1]

    # Compute filling trajectory
    xf, yf = fill(fig, w=0.145, deg=75, s=0.1)

    # Show result
    plt.plot(xf, yf, 'r')
    plt.plot(xc, yc, 'b')
    plt.plot([xc[0], xf[0]], [yc[0], yf[0]], 'g')
    plt.axis('equal')
    plt.show()

    # Write to GCODE
    allx = xc.tolist() + [xc[0]] + xf
    ally = yc.tolist() + [yc[0]] + yf
    gcode(allx, ally, 'proveta.gcode')
