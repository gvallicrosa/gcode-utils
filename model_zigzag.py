"""Create zigzag model for laser sintering tests."""

# Dades
ds = [0.142, 0.127, 0.112]  # separacions entre linies
fs = [5000, 4000, 3000, 2000]  # velocitat
ps = [40, 50, 60]  # potencies

xs = list()
ys = list()
fh = file('model-zigzag.gcode', 'w')
line = 'N5 M158\nN10 G90 O1\nN20 S4000 M3\n'
fh.write(line)
N = 25
y = 5  # Y inicial
for p in ps:
    for d in ds:
        for f in fs:
            x = 5  # X inicial
            line = 'N{:d} G00 X{:.3f} Y{:.3f}\n'.format(N, x, y)
            fh.write(line)
            N += 5
            line = 'N{:d} M00\n'.format(N)
            fh.write(line)
            N += 5
            xs = list()
            ys = list()
            while x < 245:
                xs.append(x)
                ys.append(y)
                y += 5
                xs.append(x)
                ys.append(y)
                x += d
                xs.append(x)
                ys.append(y)
                y -= 5
                xs.append(x)
                ys.append(y)
                x += d
                xs.append(x)
                ys.append(y)
            for i in range(len(xs)):
                if i == 0:
                    line = 'N{:d} G01 X{:.3f} Y{:.3f} Z0 F{:d}\n'.format(
                        N, xs[i], ys[i], f)
                    fh.write(line)
                    N += 5
                else:
                    line = 'N{:d} X{:.3f} Y{:.3f}\n'.format(N, xs[i], ys[i])
                    fh.write(line)
                    N += 5
            line = 'N{:d} M00'.format(N)
            fh.write(line)
            N += 5
            y += 7
line = 'N{:d} M30'.format(N)
