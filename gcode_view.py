#!/usr/bin/env python

"""Read and show a GCODE file."""

import sys
import vtk

from gcode_read import read_gcode
from vtk_line_plotter import VtkLinePlotter


def generate_paths(points):
    """Generate paths from points."""
    color = 0  # red
    plotter = VtkLinePlotter()
    for i in range(len(points) - 1):
        plotter.plot_line(points[i], points[i + 1], color)
        color += 1.0 / (len(points) + 1)
    polydata, mapper, actor = plotter.create_actor()
    return actor

if __name__ == '__main__':

    # Check inputs
    if len(sys.argv) < 2:
        print('Usage:\n  {:s} filename'.format(sys.argv[0]))
        exit(1)

    # Get the points
    points = read_gcode(sys.argv[1])

    # Get the 3D actor
    actor = generate_paths(points)

    # Rendering
    ren = vtk.vtkRenderer()
    ren.SetBackground(0, 0, 0)
    ren.AddActor(actor)
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
    renWin.SetSize(800, 600)
    # Create an interactor
    iren = vtk.vtkRenderWindowInteractor()
    renWin.SetInteractor(iren)
    # Create my interactor style
    style = vtk.vtkInteractorStyleTrackballCamera()
    iren.SetInteractorStyle(style)
    # Initialize and enter interactive mode
    iren.Initialize()
    iren.Start()
