#!/usr/bin/env python

"""Read GCODE from a provided file."""

import codecs
import sys


def read_gcode(fname):
    """Interpret the contents of a GCODE file."""
    fh = None
    points = list()
    try:
        fh = codecs.open(unicode(fname), "rU", 'UTF8')
        fo = file('points.txt', 'w')
        text = fh.readlines()
        x = y = z = 0
        for line in text[24:]:  # avoiding useless information
            if True:  # 'G01' in line: #G
                if 'X' in line:  # GX
                    trash, line = line.split('X')
                    if 'Y' in line:  # GXY
                        x, line = line.split('Y')
                        x = float(x)
                        if 'Z' in line:  # GXYZ
                            y, line = line.split('Z')
                            y = float(y)
                            if 'F' in line:  # GXYZF
                                z, line = line.split('F')
                                z = float(z)
                            else:  # GXYZ
                                z = float(line)
                        elif 'F' in line:  # GXYF
                            y, line = line.split('F')
                            y = float(y)
                        else:  # GXY
                            y = float(line)
                    elif 'Z' in line:  # GXZ
                        x, line = line.split('Z')
                        x = float(x)
                        if 'F' in line:  # GXZF
                            z, line = line.split('F')
                            z = float(z)
                        else:  # GXZ
                            z = float(line)
                    elif 'F' in line:  # GXF
                        x, line = line.split('F')
                        x = float(line)
                    else:  # GX
                        x = float(line)
                elif 'Y' in line:  # GY
                    trash, line = line.split('Y')
                    if 'Z' in line:  # GYZ
                        y, line = line.split('Z')
                        y = float(y)
                        if 'F' in line:  # GYZF
                            z, line = line.split('F')
                            z = float(z)
                        else:  # GYZ
                            z = float(line)
                    elif 'F' in line:  # GYF
                        y, line = line.split('F')
                        y = float(y)
                    else:  # GY
                        y = float(line)
                if 'Z' in line:  # GZ
                    trash, line = line.split('Z')
                    if 'F' in line:  # GZF
                        z, line = line.split('F')
                        z = float(z)
                    else:  # GZ
                        z = float(line)
                print >> fo, '%s %s %s' % (x, y, z)
                vec = [x, y, z]
                points.append(vec)
    finally:
        if fh is not None:
            fh.close()
        return points

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Usage:\n  {:s} filename'.format(sys.argv[0]))
        exit(1)
    read_gcode(sys.argv[1])
