"""Helper tools for slicing."""

# import math
from shapely.geometry import LineString


class Vec(object):
    """Vector class."""

    def __init__(self, data):
        """Constructor."""
        self.x = data[0]
        self.y = data[1]
        self.z = data[2]

    def __str__(self):
        """String representation."""
        return '%s %s %s' % (self.x, self.y, self.z)


def fill(fig, w, deg=90, s=None):  # missing buffer at -w/2
    """Fill a polygon with lines."""
    # fig = Polygon
    # w = path width
    # deg = degree angle fill pattern
    # s = separation of fill from contour
    if s is None:
        s = w
    # Angle to radians
    # ang = deg * math.pi / 180.0

    # Bounding box, get limits
    xmin = min(fig.buffer(-s).exterior.envelope.boundary.xy[0])
    xmax = max(fig.buffer(-s).exterior.envelope.boundary.xy[0])
    ymin = min(fig.buffer(-s).exterior.envelope.boundary.xy[1])
    ymax = max(fig.buffer(-s).exterior.envelope.boundary.xy[1])

    # Initial parameters
    x = xmin + 0.01 * s
    y = ymin
    incx = w
    incy = 0

    # Obtain intersections
    cuts = list()
    while x < xmax:  # - incx/2.0:
        # Line equation: y = x*tan(deg) + (yo - xo*tan(deg))
        line = LineString(((x, ymin), (x, ymax)))
        x += incx
        y += incy
        try:
            lines = list(line.intersection(fig.buffer(-s)))  # >1 intersection
            for line in lines:
                cuts.append(line)
        except:
            lines = line.intersection(fig.buffer(-s))  # one intersection
            cuts.append(lines)

    # Create paths from intersections
    x = list()
    y = list()
    finished = False
    reverse = False
    while not finished:
        if len(cuts) > 0:
            # Add the first line and erase it !!!!!!
            # need to check which point to take first
            nextline = cuts.pop(0)
            data = nextline.xy
            if reverse:
                lastx = data[0][0]
                lasty = data[1][0]
                x.append(lastx)
                y.append(lasty)
                lastx = data[0][1]
                lasty = data[1][1]
                x.append(lastx)
                y.append(lasty)
            else:
                lastx = data[0][1]
                lasty = data[1][1]
                x.append(lastx)
                y.append(lasty)
                lastx = data[0][0]
                lasty = data[1][0]
                x.append(lastx)
                y.append(lasty)

            reverse = not reverse
        else:
            finished = True

    return x, y


def contour(polygon):
    """Obtain coordinates of contour."""
    allx = list()
    ally = list()
    contour = polygon.exterior.xy
    x = contour[0]
    y = contour[1]
    xs = list()
    ys = list()
    for i in range(len(x)):
        xs.append(x[i])
        ys.append(y[i])
    allx.append(xs)
    ally.append(ys)

    holes = polygon.interiors
    if len(holes) > 0:
        for hole in holes:
            x = hole.xy[0]
            y = hole.xy[1]
            xs = list()
            ys = list()
            for i in range(len(x)):
                xs.append(x[i])
                ys.append(y[i])
            allx.append(xs)
            ally.append(ys)
    return allx, ally
