"""Create a stent model 2."""

import math
import numpy as np

##############
# Parameters #
##############
r = 8     # radius
n = 14    # number of layers per ring
N = 12    # number of rings
h = 0.3   # path height
w = 0.6   # path width
yd = 6    # distance from circle center to straight wall
L = N * h * n  # total lenght of the stent

# All saved points
allx = list()
ally = list()
allz = list()

xs = list()
ys = list()
zs = list()
z = 0
revers = False

ax = 0  # Layer unions
bx = 0  # Center desviation between rings
count = 1  # First layer of the ring
right = False  # Start as left ring

while z < L:
    # Look if next ring
    if count == 15:
        count = 1
        right = not right
        if right:
            bx = 3 * w
        else:
            bx = 0

    # Look if has to be moved
    if count == 1:
        ax = w
    elif count == 2:
        ax = w / 2.0
    elif count == 13:
        ax = w / 2.0
    elif count == 14:
        ax = w
    else:
        ax = 0

    if right:
        ax = -ax

    # Initial point in x**2 + y**2 = r**2
    y = yd
    x = math.sqrt(r ** 2 - y ** 2)
    xs.append(x + ax + bx)
    ys.append(y)
    zs.append(z)

    # Second point
    x = -x
    xs.append(x + ax + bx)
    ys.append(y)
    zs.append(z)

    # First circle loop
    ang = np.rad2deg(math.asin(yd / r))
    for a in np.arange(180 - ang, 360 + ang, 10):
        a = np.deg2rad(a)
        x = r * math.cos(a) + ax + bx
        y = r * math.sin(a)
        xs.append(x)
        ys.append(y)
        zs.append(z)
    ang = np.deg2rad(ang)
    x = r * math.cos(ang) + ax + bx
    y = r * math.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # Second circle loop
    r2 = r + w
    ang = math.asin(yd / r2)
    x = r2 * math.cos(ang) + ax + bx
    y = r2 * math.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)
    ang = np.rad2deg(ang)
    angles = np.arange(180 - ang, 360 + ang, 10)
    angles.reverse()
    for a in angles:
        a = np.deg2rad(a)
        x = r2 * math.cos(a) + ax + bx
        y = r2 * math.sin(a)
        xs.append(x)
        ys.append(y)
        zs.append(z)
    ang = 180 - ang
    ang = np.deg2rad(ang)
    x = r2 * math.cos(ang) + ax + bx
    y = r2 * math.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # Third circle loop
    r2 = r + 2 * w
    ang = math.asin(6.0 / r2)
    x = r2 * math.cos(math.pi - ang) + ax + bx
    y = r2 * math.sin(math.pi - ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)
    ang = np.rad2deg(ang)
    angles = np.arange(180 - ang, 360 + ang, 10)
    for a in angles:
        a = np.deg2rad(a)
        x = r2 * math.cos(a) + ax + bx
        y = r2 * math.sin(a)
        xs.append(x)
        ys.append(y)
        zs.append(z)
    ang = np.deg2rad(ang)
    x = r2 * math.cos(ang) + ax + bx
    y = r2 * math.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    if revers:
        xs.reverse()
        ys.reverse()
        zs.reverse()

    revers = not revers

    allx += xs
    ally += ys
    allz += zs

    xs = list()
    ys = list()
    zs = list()

    z += h
    count += 1

fh = file('model2.txt', 'w')
for i in range(len(allx)):
    line = '{:.3f} {:.3f} {:.3f}\n'.format(allx[i], ally[i], allz[i])
    fh.write(line)
fh.close()
