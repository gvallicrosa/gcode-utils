"""Helper to plot lines in VTK."""

import vtk


class VtkLinePlotter(object):
    """Class to plot lines in VTK."""

    def __init__(self):
        """Constructor."""
        self.m_scalarMin = 0.0
        self.m_scalarMax = 1.0
        self.m_lookupTable = None
        self.m_curPointID = 0
        self.m_allLineWidth = 1

        self.m_points = vtk.vtkPoints()
        self.m_lines = vtk.vtkCellArray()
        self.m_lineScalars = vtk.vtkFloatArray()

    def set_scalar_range(self, minval, maxval):
        """Set the scalar range."""
        self.m_scalarMin = minval
        self.m_scalarMax = maxval

    def set_lookup_table(self, table):
        """Set lookup table."""
        self.m_lookupTable = table

    def plot_line(self, m, n, scalar):
        """Plot a line from points."""
        self.m_lineScalars.SetNumberOfComponents(1)
        self.m_points.InsertNextPoint(m)
        self.m_lineScalars.InsertNextTuple1(scalar)
        self.m_points.InsertNextPoint(n)
        self.m_lineScalars.InsertNextTuple1(scalar)
        self.m_lines.InsertNextCell(2)
        self.m_lines.InsertCellPoint(self.m_curPointID)
        self.m_lines.InsertCellPoint(self.m_curPointID + 1)
        self.m_curPointID += 2

    def set_all_line_width(self, width):
        """Set line width."""
        self.m_allLineWidth = width

    def create_poly_data(self):
        """Create poly data."""
        polyData = vtk.vtkPolyData()
        polyData.SetPoints(self.m_points)
        polyData.SetLines(self.m_lines)
        polyData.GetPointData().SetScalars(self.m_lineScalars)
        return polyData

    def create_actor(self):
        """Create poly data."""
        polydata = self.create_poly_data()
        # create a color lookup table
        if self.m_lookupTable is None:
            self.m_lookupTable = vtk.vtkLookupTable()
        # create mapper
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInput(self.create_poly_data())
        mapper.SetLookupTable(self.m_lookupTable)
        mapper.SetColorModeToMapScalars()
        mapper.SetScalarRange(self.m_scalarMin, self.m_scalarMax)
        mapper.SetScalarModeToUsePointData()
        # create actor
        actor = vtk.vtkActor()
        actor.SetMapper(mapper)
        actor.GetProperty().SetLineWidth(self.m_allLineWidth)
        return polydata, mapper, actor
