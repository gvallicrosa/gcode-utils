"""Create a cylindrical tube model."""

import math
import numpy as np

##############
# Parameters #
##############
r = 8  # radius
L = 60   # total length
yd = 6    # distance from circle center to straight wall
h = 0.3   # path height
w = 0.6   # path width

# Variables to save all points
allx = list()
ally = list()
allz = list()

# Temporal varibles
xs = list()
ys = list()
zs = list()

# Starting values
z = 0

while z < L:

    # Initial point in circumference x**2 + y**2 = r**2
    y = yd
    x = math.sqrt(r ** 2 - y ** 2)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # Second point
    x = -x
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # First circle loop
    ang = np.rad2deg(math.asin(6.0 / r))
    for a in np.arange(180 - ang, 360 + ang, 10):
        a = np.deg2rad(a)
        x = r * math.cos(a)
        y = r * math.sin(a)
        xs.append(x)
        ys.append(y)
        zs.append(z)

    # Last point of the loop
    ang = np.deg2rad(ang)
    x = r * math.cos(ang)
    y = r * math.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    allx += xs
    ally += ys
    allz += zs

    xs = list()
    ys = list()
    zs = list()

    z += h

# Write path to a text file
fh = file('tube.txt', 'w')
for i in range(len(allx)):
    line = '{:.3f} {:.3f} {:.3f}\n'.format(allx[i], ally[i], allz[i])
    fh.write(line)
fh.close()
