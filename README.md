# Contents #

This repo contains some old code from my work during the final Bachelor project.

GCODE reader and plotter.
Only accepts G01 commands (no curves like G02 and G03).

## Requeriments ##

* python-matplotlib
* python-numpy
* python-pyvtk
* python-shapely
* python-vtk

## Usage ##

gcode_read.py - reads a GCODE from a file
gcode_view.py -> reads and plots a GCODE from a file
