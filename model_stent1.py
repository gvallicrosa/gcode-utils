"""Create a stent model 1."""

import numpy as np

##############
# Parameters #
##############
rmin = 8  # minimum radius
rmax = 10  # maximum radius
l = 9     # distance between rings
L = l * 2   # total length
yd = 6    # distance from circle center to straight wall
h = 0.3   # path height
w = 0.6   # path width

# Variables to save all points
allx = list()
ally = list()
allz = list()

# Temporal varibles
xs = list()
ys = list()
zs = list()

# Starting values
z = 0
revers = False

while z < L:
    # Calculate r from z
    r = (rmin + rmax) / 2.0 + (rmax - rmin) / 2.0 * np.cos(z * 2.0 * np.pi / l)

    # Initial point in circumference x**2 + y**2 = r**2
    y = yd
    x = np.sqrt(r ** 2 - y ** 2)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # Second point
    x = -x
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # First circle loop
    ang = np.rad2deg(np.arcsin(6.0 / r))
    for a in np.arange(180 - ang, 360 + ang, 10):
        a = np.deg2rad(a)
        x = r * np.cos(a)
        y = r * np.sin(a)
        xs.append(x)
        ys.append(y)
        zs.append(z)

    # Last point of the loop
    ang = np.deg2rad(ang)
    x = r * np.cos(ang)
    y = r * np.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    # Second circle loop
    r2 = r + w
    ang = np.arcsin(6.0 / r2)
    x = r2 * np.cos(ang)
    y = r2 * np.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)
    ang = np.rad2deg(ang)
    angles = np.arange(180 - ang, 360 + ang, 10)
    angles.reverse()
    for a in angles:
        a = np.deg2rad(a)
        x = r2 * np.cos(a)
        y = r2 * np.sin(a)
        xs.append(x)
        ys.append(y)
        zs.append(z)

    # Last point of the loop
    ang = 180 - ang
    ang = np.deg2rad(ang)
    x = r2 * np.cos(ang)
    y = r2 * np.sin(ang)
    xs.append(x)
    ys.append(y)
    zs.append(z)

    if revers:
        xs.reverse()
        ys.reverse()
        zs.reverse()

    revers = not revers

    allx += xs
    ally += ys
    allz += zs

    xs = list()
    ys = list()
    zs = list()

    z += h

# Write path to a text file
fh = file('model-stent1.txt', 'w')
for i in range(len(allx)):
    line = '{:.3f} {:.3f} {:.3f}\n'.format(allx[i], ally[i], allz[i])
    fh.write(line)
fh.close()
