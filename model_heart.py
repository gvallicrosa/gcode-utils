"""Create heart model for chocolate."""

import numpy as np

from scipy.optimize import newton
from shapely.geometry import Polygon
from planner import fill

##############
# Parameters #
##############
# Machine
h = 1.1
w = 1.2

# Figure
R = 35
xR = -19.1463
yR = 29.2988

r = 7.5
xr = 7.5
yr = 22.5


def func(y, R, r):
    """Circle function."""
    xR = -14.21
    yR = 20.57
    xr = 5
    yr = 15
    # print y, (r**2 - (y - yr)**2) + xr, (R**2 - (y - yR)**2) + xR
    num = (np.sqrt(r ** 2 - (y - yr) ** 2) + xr) - \
        (np.sqrt(R ** 2 - (y - yR) ** 2) + xR)
    # print num
    return num - 1e-6


def half(R, r, xR=-14.21, yR=20.57, xr=5, yr=15):
    """Half heart function."""
    # Base
    rb = 5
    # Rb = 25

    # (x-xo)**2 + (y-yo)**2 = r**2
    resx = list()
    resy = list()

    if not r == rb:
        for ang in np.arange(270, 361, 10):
            ri = rb - r
            ang = np.deg2rad(ang)
            x = 0 + ri * np.cos(ang)
            y = 15 + ri * np.sin(ang)
            resx.append(x)
            resy.append(y)
    else:
        x = 0
        y = 15
        resx.append(x)
        resy.append(y)

    # Look for tangent point
    y = newton(func, x0=20, fprime=None, args=(R, r), tol=1e-6, maxiter=50)
    x = np.sqrt(r ** 2 - (y - yr) ** 2) + xr

    # Calc angles
    angr = - atan(abs(y - yr) / abs(x - xr))
    angR2 = - atan(abs(y - yR) / abs(x - xR))

    xa = 0
    ya = - np.sqrt(R ** 2 - (xa - xR) ** 2) + yR
    angR1 = - np.arctan(abs(yR - ya) / abs(xR))

    anglesr = np.arange(np.rad2deg(angr), 180, 10)
    anglesr.reverse()

    anglesR = arange(np.rad2deg(angR1), np.rad2deg(angR2))
    anglesR.reverse()

    # First point
    x = xr - r
    y = yr
    resx.append(x)
    resy.append(y)

    # Calc points
    for ang in anglesr:
        ang = np.deg2rad(ang)
        x = xr + r * np.cos(ang)
        y = yr + r * np.sin(ang)
        resx.append(x)
        resy.append(y)

    for ang in anglesR:
        ang = np.deg2rad(ang)
        x = xR + R * np.cos(ang)
        y = yR + R * np.sin(ang)
        resx.append(x)
        resy.append(y)

    return resx, resy


def full(R, r):
    """Create full heart."""
    # All saved points
    allx = list()
    ally = list()

    # Calc half
    xs, ys = half(R, r)
    # Add and mirror to have a full one
    allx += xs
    ally += ys
    xs = list(-array(xs))
    xs.reverse()
    ys.reverse()
    allx += xs
    ally += ys
    return allx, ally


def filled(R, r, w, onlyfill=False):
    """Create filled heart."""
    global idx
    # Calc contour
    xc, yc = full(R, r)

    # Fill contour
    points = list()
    for i in range(len(xc)):
        point = (xc[i], yc[i])
        points.append(point)

    fig = Polygon(points)
    xf, yf = fill(fig, 0.95 * w, 90, 0.8 * w)

    # Look for nearest point
    xp = xf[0]
    yp = yf[0]
    distm = 100
    idx = -1
    for i in range(len(xc)):
        x = xc[i]
        y = yc[i]
        dist = np.sqrt((xp - x) ** 2 + (yp - y) ** 2)
        if dist < distm:
            idx = i
            distm = dist

    # Reorder contour points
    xc = xc[idx:] + xc[:idx]
    yc = yc[idx:] + yc[:idx]

    # Save them all
    if onlyfill:
        allx = xf
        ally = yf
    else:
        allx = xc + xf
        ally = yc + yf

    return allx, ally

# MAIN PROGRAM
allx = list()
ally = list()
allz = list()

z = 0
n = 1
reverse = False
while n <= 2:  # 2 layers of filled chocolate
    xs, ys = filled(R, r, w)
    zs = [z] * len(xs)
    if reverse:
        xs.reverse()
        ys.reverse()
    reverse = not reverse
    allx += xs
    ally += ys
    allz += zs

    n += 1
    z += h

n = 1
zp = z
while n <= 2:  # 2 layers of walls
    i = 1
    xs = list()
    ys = list()
    Rs = R
    rs = r

    while i <= 2:  # 2 path wall
        x, y = full(Rs, rs)
        x = x[idx:] + x[:idx]
        y = y[idx:] + y[:idx]
        xs += x
        ys += y

        rs -= w
        Rs -= w
        i += 1

    if reverse:
        xs.reverse()
        ys.reverse()
    zs = [z] * len(xs)
    allx += xs
    ally += ys
    allz += zs

    n += 1
    z += h

fh = file('model-heart1.txt', 'w')
for i in range(len(allx)):
    line = '{:.3f} {:.3f} {:.3f}\n'.format(allx[i], ally[i], allz[i])
    fh.write(line)
fh.close()

allx = list()
ally = list()
allz = list()

n = 1
z = zp
while n <= 2:  # 2 layers of something inside
    xs, ys = filled(R - w, r - w, w, True)
    zs = [z] * len(xs)
    if reverse:
        xs.reverse()
        ys.reverse()
    reverse = not reverse
    allx += xs
    ally += ys
    allz += zs

    n += 1
    z += h

fh = file('model-heart2.txt', 'w')
for i in range(len(allx)):
    line = '{:.3f} {:.3f} {:.3f}\n'.format(allx[i], ally[i], allz[i])
    fh.write(line)
fh.close()

allx = list()
ally = list()
allz = list()

n = 1
while n <= 2:  # 2 layers of chocolate to close
    xs, ys = filled(R, r, w)
    zs = [z] * len(xs)
    if reverse:
        xs.reverse()
        ys.reverse()
    reverse = not reverse
    allx += xs
    ally += ys
    allz += zs

    n += 1
    z += h

fh = file('model-heart3.txt', 'w')
for i in range(len(allx)):
    line = '{:.3f} {:.3f} {:.3f}\n'.format(allx[i], ally[i], allz[i])
    fh.write(line)
fh.close()
